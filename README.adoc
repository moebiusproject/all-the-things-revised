= All The Things Revised
:toc: right
:toc-title:
:sectanchors:
// :sectnums:
:idprefix:
:prefix: all-the-things-revised
:imagesdir: {prefix}/docs

A small collection of tweaks, to suite the tastes of its author, and with plans
to delve into (humble) item and spell changes.

== Miscellaneous changes

[[ai_script]]
=== suy's party AI script

It's my honest take on an AI script for people who hate AI scripts and want to
do it all manually, like myself, as I have disliked party scripts pretty much
always, favoring full control.
But I started conceding little things here and there, experimenting with the
more minimalist's scripts that come with the game.
I only attempt to do the minimal "quality of life" automation, nothing else.

This script combines the features I wanted from the default "Smart AI" and the
different "controlled" AI scripts ("Bard controlled", "Thief controlled", etc.)
that are built in, with some simple refinements on attacking, coming from other
advanced scripts, and with the help of the community.

TIP: Installing a party AI script like this only adds an option to choose it
later in game (during game play, check: Record, Customize, Script). You can
later on decide if you want to use it or not, or in which characters to do so,
etc.

The script supports two keys to set two states on each party member, and let it
decide what to do.

NOTE: The keys are not yet customizable during the installation time. You will
have to open the file `suy-ai.baf` (before installation) and do a search and
replace with a text editor. Look for `HotKey` in the code, and use the keys of
your choice. The documentation below assumes the defaults are left.

O key ("offensive")::
It changes whether a character will attack with the currently equipped weapon
when enemies are present. Each press alternates between the following modes
(which will be displayed on screen):
* Attacking enemies (see below for the details)
* Not attacking enemies

G key ("general mode")::
This is dependent on the class of the character, and some classes get nothing
out of it.
This is the default key for "Return to Game" when you are in a menu, so it is
typically unused anyway.
The feature is to make characters do something useful that they have "for free"
when doing area exploration, without the player remembering having to click in
the modal ability in between fights.
+
--
Pressing the key goes from one mode to the next, or back to the first:

. Manual
. Attempt to hide/Sing constantly
. Detect traps and illusions/Sing only during combat

This way a Bard can choose to be leveraging their song constantly, even without
enemies, so at the first round of combat the party will be already buffed with
it for at least a few seconds, and even if the very first action of the Bard is
cancelling the song to make an attack, cast a spell, etc.
A Jester, whose song is only useful with enemies nearby, can choose the other
mode to sing only with hostile creatures (or any other Bard, if you prefer not
to spam the "Singing Bard Song" messages).

Or, more importantly, have a Thief always activate the Find Traps ability, which
is very useful if you are looting a room with multiple locks and traps, as each
trap and lock removal stops the trap finding and forces you to remember to
activate it yourself **again**.
This has caused me so many annoying episodes that is one of the reasons why I
started using an AI script in the first place.

Combined with the <<rogues_luck,Rogue's Luck component of this very same mod>>,
attempting to hide constantly is also a small improvement to get a free boost of
the first round of combat.
--

The script has the expected niceties that give you just a bit of quality of
life, but still keeping as much control as you might want.
Any character with this script:

* will only attack if their weapon is in range.
  If your melee attacker on the front line sees enemies at the edge of the fog
  of war, it will not walk right away walk to them to attack, to prevent
  triggering a trap.
* will only move while is engaged with some enemy, which is the same as if you
  clicked manually on an enemy to attack.
* will only attempt an ability if it's not doing something else.
  If you made the character detect traps, it won't try to hide in shadows, or
  the other way around, even if that's what they attempt to do when idle.
  Of course it won't attempt anything if it's just engaged in combat, or casting
  a spell, etc.
* will avoid hiding in shadows if an ally has scheduled something, to prevent
  that healing spells get wasted on a character that goes into shadows because
  there are no enemies in sight.
+
WARNING: This still is very limited, as it's not possible to do this properly
with just a script, so beware.
* will skip starting to attack or sing if the character is under invisibility or
  Sanctuary.
* will avoid attacking hostile creatures which are charmed party members or
  innocents that cause reputation loss.
* will prioritize enemies who are not in panic, stunned, helpless, etc.
  The character will still go for them if all in range are in such precarious
  state, so you can finish off the trash mobs "hands free".

In summary: it's as unobtrusive as I could make it, attempting only to do
quality of life actions that relieve the pressure on the player.
And remember that the two features can be disabled with the key shortcut on a
character by character basis.




//==============================================================================

== Game Mechanics

[[facestab]]
=== Facestab: Backstab from any angle

Enemies can perform a backstab without having to hit you from the back.
We want to do the same: positioning is tedious, and sometimes the attack can
land faster than you can realize that the enemy turned or moved slightly.

This can't be done as cleanly as the enemy can do it, so it works based on a
workaround.
Once a player is hidden in shadows or invisible, it will gain for a very short
period of time the Assassination ability, which allows to backstab from any
position.
The ability is refreshed during the time the character is invisible.

TIP: If you have many attacks per round, you might be able to pull off more than
one backstab in a row, but it will be unlikely, and late game.
This is not exactly intentional, and future versions might make it harder or
downright impossible to do, but given that it is not easy, is balanced enough.

I want to give a special thank you to
https://forums.beamdog.com/post/quote/66096/Comment_903267[semiticgoddess for
her comment on the Beamdog forum], for the idea of using the Assassination
effect to implement this, and to WeareTheDraiken for pointing me there. Long
ago, I tried to implement this by reverse engineering the engine (without
success), and I never heard of attempting such simple workaround before.



[[rogues_luck]]
=== Rogue's Luck: Improve attacks after stealth/invisibility

An improvement for pure-Rogue only characters (that is, Bards and single class
Thieves, of any kit, but without a second class from multi or dual classing).
The characters that qualify will get better attacks when coming out of stealth
or invisibility, for a duration of 5 seconds.
Those attacks can be backstabs or not, so a Bard or Swashbuckler (who lack the
ability to backstab), or Thief with an unsuitable weapon (like in a ranged
attack) will still receive the benefits.

The effects of Rogue's Luck are:

* No critical miss. A roll of 1 will still likely fail because is a very low
  attack roll, but if the Rogue has a good chance to hit, and/or the opponent is
  heavily penalized, it can still render a hit with a 1, and it won't be an
  automatic miss.
+
NOTE: The default of the game is a critical miss on a 1, and this mod component
only shifts it to 0, for the behavior explained above.
If an hypothetical second mod penalizes the critical miss probability of the
character in the opposite way, it's possible that a 1 is still a sure failure,
as this mod only gives an improvement of 1, so it's the behavior of both mods
stacking.
This is again, hypothetical, as I've not found such a mod.
* A "lucky" attack roll, with an improved minimal roll, and better chance of
  scoring the higher roll.
  For people who know the
  https://forums.beamdog.com/discussion/61569/luck-what-it-is-and-how-it-works/[luck
  mechanic], this is the same, but affecting only the damage of outgoing
  attacks.
  So it's like Kai or Offensive Spin, but much toned down (those give a +20 to
  ensure maximum damage every hit, while this ability just gives +2 at level 1,
  and scales up to +8 at level 19, at a rate of 1 per each 3 levels).
+
TIP: To explain briefly, a bonus of this kind improves a roll by the amount of
the bonus, but not beyond the maximum of the natural, unmodified roll.
So a 1d4 dagger will normally do 1, 2, 3 or 4 damage, but when using it after
stealth, and for the next 5 seconds, a level 1 character will do 3, 4, 4 or 4
points of damage.

.Progression of damage bonus with Rogue level
[width=30%, role=center]
|===
|Level   |Bonus

|1-3     |+2
|4-6     |+3
|7-9     |+4
|10-12   |+5
|13-15   |+6
|16-18   |+7
|>=19    |+8
|===

This makes Rogues *feel reliable* in what is supposed to be one of their
signature moves.
They will still not easily kill enemies in a single hit, but combining
this with the <<facestab,Facestab component>> and the feature to hide in
shadows constantly of <<ai_script,a party AI script like mine>>, they will be
able to have a bit of an edge that makes them special at the start of a battle,
and will make your investment in stealth skills feel a bit more valuable.

image::good-backstab.png[]



[[linear_proficiency_points]]
=== Worthwhile Proficiency Points: Buff 1st and 4th proficiency point in a weapon

This component makes two simple changes, one for the first proficiency point in
a weapon (the only point that many classes can allocate, also known as just
"Proficient"), and the fourth one (the maximum that Dwarven Defenders can put in
their favored weapons, also known as "High Mastery"):

. The first point will now give +1 to hit (instead of nothing).
. The fourth point will now give +5 to damage (instead of +4).

That's it. That's all this component does.

This changes are very simple, but aim to make every proficiency point to seem
much more worthwhile, and to balance classes and the gameplay to
make them more satisfying in the following ways:

* Early combat gets let's tedious if everyone gets (at worst) +1 to hit.
* The gap between classes who can only put one point or two is shortened.
  All the classes that can start with two points in a weapon still are getting
  quite the advantage: +2 to damage and (if applicable) higher attacks per
  round.
* Dwarven Defenders are not losing *that* much compared to unkitted Fighters,
  and makes their 4th point a more noticeable improvement with the 3rd.
  Characters who still can reach the 5th point can do so with a less bumpy
  progression curve.
* Fighter>Thief dual classed characters don't need to reach the 5th point in a
  weapon that they only want to use for backstabbing, as the 4th one is enough
  to deal maximum damage in a single attack.
  For example, you could build a Fighter>Thief character whose main weapons are
  Bastard Swords in one hand, and Scimitars in the other.
  With this change, the 4th proficiency point is enough for maximum damage from
  your off-hand, and when you need to move the scimitar to the main hand for a
  backstab, the damage will also be the maximum.
  Likewise if you wanted to invest proficiency points in Quarterstaff for the
  backstabbing only.




//==============================================================================

== Roadmap

A lot of other small changes are planned.
The changes will follow the motto of "small change, big impact", and the theme
is making more classes, weapons or weapon categories more interesting (like
making **all** spears have an extra attack, to compensate for their low damage),
or adding effects to shields in Shadows of Amn to make them compelling to use,
instead of relying on dual wielding that much.

Likewise for spells.
It should be interesting, fun and/or useful to use as many spells as possible.
Note, however, that my goal is not just to play compatible mods, but to play a
**consistent** modded setup, which means that if this mod changes the way spells
are used or their usefulness, I would feel very pressured to make the enemies
aware of such changes, and that involves reviewing spell picks or AI changes, at
least with SCS.




//==============================================================================

== License

All code is licensed under the terms of the MIT License, which are the following:

[quote]
----
Copyright (c) 2023-2024 Alejandro Exojo

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
----

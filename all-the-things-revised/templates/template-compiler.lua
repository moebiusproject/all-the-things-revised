#!/usr/bin/lua

local etlua = require("etlua")

local function load_template(filename)
    local file = assert(io.open(filename, "r"))
    local template = file:read("*all")
    return etlua.compile(template)
end

local templates = {}

local function render(template_name, data)
    -- Add the render function to the data table so it's accessible inside
    -- templates.
    data = data or {}
    data.render = render

    local cached = templates[template_name]
    if not cached then
        cached = load_template(template_name)
        templates[template_name] = cached
    end
    return cached(data)
end

print(render(arg[1]))
